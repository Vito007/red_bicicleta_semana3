var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], 
        index:{ type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createIntance = function(code, color, modelo, ubicacion){
    return new this ({
        code,
        color,
        modelo,
        ubicacion
    })
}

bicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb); 
};

bicicletaSchema.statics.add = function(bici, cb){
    this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function(code, cb){
    return this.findOne({code: code}, cb);
};

bicicletaSchema.statics.removeByCode = function(code, cb){
    return this.deleteOne({code: code}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);


// var Bicicleta = function (id, color, modelo, ubicacion){
//     this.id = id;
//     this.modelo = modelo;
//     this.color = color;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function(){
//     return 'id: '+ this.id; 
// }

// Bicicleta.allBicis = [];

// Bicicleta.add = function (bici) {
//    Bicicleta.allBicis.push(bici); 
// }

// Bicicleta.findById = function(biciId){
//     var newBici = Bicicleta.allBicis.find(x => x.id == biciId);
//     if(newBici)
//         return newBici
//     else
//         throw new Error(`No existe una bicicleta con el id ${biciId}`);
// }

// Bicicleta.removeById = function(biciId){
//     // var biciFind = this.findById(biciId);
//     for (let index = 0; index < Bicicleta.allBicis.length; index++) {
//         if(Bicicleta.allBicis[index].id == biciId){
//             Bicicleta.allBicis.splice(index, 1);
//             break;
//         }
//     }
// }


// // var a = new Bicicleta(1, 'rojo', 'montaña', [-38.0019066, -57.5810998]);
// // var b = new Bicicleta(2, 'verde', 'playera', [-38.0095953, -57.590981]);

// // Bicicleta.add(a);
// // Bicicleta.add(b);

// module.exports = Bicicleta;