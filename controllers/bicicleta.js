var Bicicleta = require('../models/bicicletas');

// exports.Bicicleta_list = function(req, res){
//     res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
// }

exports.Bicicleta_list = function(req,res){ 
    Bicicleta.allBicis(function(err, bicis){
        bicis_all = bicis;
        res.render('bicicletas/index', {bicis: bicis})    
    }); 
    
}
exports.Bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}
exports.Bicicleta_create_post = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion= [req.body.latitud, req.body.longitud];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

// No usamos una pagina especifica para el borrado, se envia un formulario directamente de la tabla por POST
// exports.Bicicleta_delete_get = function(req, res){
//     res.render('bicicletas/delete');
// }

exports.Bicicleta_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}

exports.Bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update', {bici});
}

exports.Bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    // Esto seria mejor hacerlo en el Modelo, creando una funcion y llamandola, que retorne si la modificacion tuvo exito o no y que de eso dependa la redireccion
    bici.id = req.body.id;
    bici.modelo = req.body.modelo;
    bici.color = req.body.color;
    bici.ubicacion = [req.body.latitud, req.body.longitud];

    res.redirect('/bicicletas');
}